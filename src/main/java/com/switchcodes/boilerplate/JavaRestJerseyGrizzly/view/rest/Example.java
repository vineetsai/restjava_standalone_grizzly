package com.switchcodes.boilerplate.JavaRestJerseyGrizzly.view.rest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Rest Interface for Example endpoint
 * 
 * @author Vineet Saini Dec 27, 
 * 2015 Example.java
 */
@Path("api/v1/example")
public class Example {

  private static final Logger logger = LoggerFactory.getLogger(Example.class);

  @POST
  @Path("/")
  @Produces(MediaType.APPLICATION_JSON)
  public Response allocatePool(String requestString) {
    logger.debug("Request String: " + requestString);
    return Response.status(Status.ACCEPTED).entity("Good").build();
  }

  @DELETE
  @Path("/example/{exampleId}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response releasePool(@PathParam("exampleId") int serviceId) {
    return Response.status(Status.OK).entity("Deleted").build();
  }

  @GET
  @Path("/example/{exampleId}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response statusPool(@PathParam("exampleId") int serviceId) throws JsonProcessingException {
    return Response.status(Status.OK).entity("Here is Status:  Hello").build();
  }

}
