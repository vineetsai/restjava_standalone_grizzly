package com.switchcodes.boilerplate.JavaRestJerseyGrizzly.main;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.switchcodes.boilerplate.JavaRestJerseyGrizzly.view.rest.Example;

/**
 * This is main loader for the service 
 * Deploy with stand alone grizzly web server
 * 
 * @author vsaini Dec 27, 2015 2015 InitProcess.java
 */
public class InitProcess {

  private static final Logger logger = LoggerFactory.getLogger(InitProcess.class);

  public static void main(String[] args) throws InterruptedException, IOException {

    // get port
    int port = 7171;

    // get host
    String host = "0.0.0.0";

    // configure jersey
    ResourceConfig config = new ResourceConfig();
    config.register(JacksonFeature.class);
    config.register(MultiPartFeature.class);
    config.register(LoggingFilter.class);
    config.register(Example.class);

    URI uri = UriBuilder.fromUri("http://" + host + "/").port(port).build();
    HttpServer server = GrizzlyHttpServerFactory.createHttpServer(uri, config);

    logger.debug(String.format("Started server on %s:%d.", host, port));
    while(true) {
      Thread.sleep(1000);
      if(System.in.available() > 0) {
        int read = System.in.read();
        System.out.println("" + read + " Closing...");
        server.shutdownNow();
        break;
      }
      // System.out.print(".");
    }
    logger.debug("Server closed.");

  }
}
